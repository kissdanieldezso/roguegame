﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueGame.Jatek.Jatekter
{
    abstract class JatekElem
    {
        int x;
        int y;
        protected JatekTer ter;

        public int X { get { return x; } }
        public int Y { get { return y; } }

        public abstract double Meret { get; }

        public JatekElem(int x, int y, JatekTer ter)
        {
            this.x = x;
            this.y = y;
            this.ter = ter;
            this.ter.Felvetel(this);
        }

        public abstract void Utkozes(JatekElem jatekElem);
    }
}
