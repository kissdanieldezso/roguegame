﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueGame.Jatek.Jatekter
{
    class JatekTer
    {
        const int MAX_ELEMSZAM = 1000;
        int elemN;
        JatekElem[] elemek;
        int meretX;
        int meretY;

        public int MeretX
        {
            get { return meretX; }            
        }

        public int MeretY
        {
            get { return meretY; }
        }

        public JatekTer(int szelesseg, int magassag)
        {
            meretX = szelesseg;
            meretY = magassag;
            elemek = new JatekElem[MAX_ELEMSZAM];
            elemN = 0;
        }

        public void Felvetel(JatekElem elem)
        {
            elemek[elemN++] = elem;            
        }

        public void Torles(JatekElem elem)
        {
            // [J1, J2, J3, J4, J5, null, null, null]
            // előre mozgatom a törlendő elem szomszédját, stb...
            // [J1, J3, J4, J5, null, null, null, null]

            // 1. fázis: megkeresem az elemet
            int idx = 0;
            while (idx < elemN && elemek[idx] != elem)
            {
                idx++;
            }
            // 2. fázis: ha benne van, felülírom a jobb szomszéddal
            if (idx < elemN)
            {
                while (idx < elemN - 1)
                {
                    elemek[idx] = elemek[idx + 1];
                    idx++;
                }
            // 3. fázis: az utolsó elem mindig null
                elemek[idx] = null;
                elemN--;
            }
        }

        public JatekElem[] MegadottHelyenLevok(int x, int y, int tavolsag)
        {
            // JatekElem[] tömbbel
            List<JatekElem> kivalogatottElemek = new List<JatekElem>();

            for (int i = 0; i < elemN; i++)
            {
                // SQRT((x-x0)^2 + (y-y0)^2)
                double tav = Math.Sqrt(Math.Pow(x - elemek[i].X, 2) + Math.Pow(y - elemek[i].Y, 2));
                if (tav <= tavolsag)
                {
                    kivalogatottElemek.Add(elemek[i]);
                }
            }
            return kivalogatottElemek.ToArray();
        }

        public JatekElem[] MegadottHelyenLevok(int x, int y)
        {
            return MegadottHelyenLevok(x, y, 0);
        }
    }
}
