﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueGame.Jatek.Jatekter
{
    abstract class MozgoJatekElem : JatekElem
    {
        private bool aktiv;

        public bool Ativ
        {
            get { return aktiv; }
            set { aktiv = value; }
        }

        public MozgoJatekElem(int x, int y, JatekTer ter) : base(x,y,ter)
        {
        }

        public void Athelyez(int ujX, int ujY)
        {
            JatekElem[] adottHelyenLevok = ter.MegadottHelyenLevok(ujX, ujY);
            int idx = 0;
            while (idx < adottHelyenLevok.Length && this.aktiv)
            {
                JatekElem elem = adottHelyenLevok[idx];
                elem.Utkozes(this);
                this.Utkozes(elem);
                idx++;
            }
        }
    }
}
